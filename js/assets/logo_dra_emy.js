(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"logo_dra_emy_atlas_1", frames: [[0,0,700,702]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.CachedBmp_1 = function() {
	this.initialize(ss["logo_dra_emy_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Tween4 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABXOeQjsgwijh+Qikh/hWjLQgfhHABhAQAChBAhg6QAPgbgDgXQgDgYgVgYQgVgWgogtIgTgWQiEiZAvjJQAdh7BthRQAwgkA4gUQA3gUAzAAQB/AABpA/QB3BGAfB7QASBKgEBIQgDAxgUBiQgEASAAApQAABFgQAhQgcA5hWAVQgCABgDAGQgFAJgBAHQAHAHAZABQAXABAcgEQAMgmAvgdQALgIALgDIADgBQAVgGAdAEQATADAdAKIAEABIBFhlQAWgeAfgPQAggPApABQBMAAA+AjQA9AjAzBIQAdApARAwQASAxAIA9QA4gPAfAVQAeAUACAyQACAdgFAlQgCAVgIArIgEAWQgDAOgMAPQgHAJgKAIQgJAIgXAHIgRAGIhSAGIgCAmQgGBIggA8QggA8g4ArQg5ArhBAWQhCAXhGAAQguAAgwgJgApFoxQhlBLgbBzQgWBeAUBTQAUBTA9BHIATAWQAlAqAXAZQA3A7gmBFQgfA1gBA6QgBA7AcBBQBVDICdB5QCcB4DqAvQC5AlCRhvQBphQALiJQACgXAAgXIAAgBQgCiFgoh3QgMgmgPggIgNgYQgCgFABgFQABgFAFgDQAFgCAFABQAFABADAEIAOAcQAPAhANAnQAnB0AECDIBOgHIAMgEQATgHAGgEQAJgHAGgJQAHgIABgHIADgWQAIgqADgUQAEgigBgbQgCgmgUgNQgMgIgUAAQgUABgaAJQgGACgGgDQgFgEgBgGQgHg/gRgxQgRgygcgoQgvhDg4ggQg5gghFAAQhGgBgkAxIhBBfQA2AVAoAYQBCAnApA1QA7BNAEBGQADAwgYAzQgOAdgrATQgyAXhOAAQgZAAgagCIAgBmQABAFgCAFQgDAEgFACIgDABQgFAAgDgDQgEgCgBgFIgjhwQh2gQiYg7QgFgCgCgFQgCgFACgFQACgEAEgDQAGgCAFACQCaA8B1AOIABAAQA3AHAwgFQA2gFAjgSQAcgOAIgSQAXgugEgrQgFg8g0hFQgngxg+gmQgsgZg8gWIAAAAIgMgEQgbgKgRgCQgYgEgRAFIgCAAQgHADgGAGQglAWgKAZQAWAGAcAMQAgAOAZANQAnAWAoAkQAUATANAPQAEAEgBAFQAAAGgEADQgEADgEAAQgGAAgEgEQgMgOgTgRQgmgjglgUQgYgNgcgNQgjgPgYgFQgbAFgaAAQgqABgPgPQgKgJACgPQADgNAGgLQAIgQAMgEQBLgSAXgtQAOgcAAhAIAAghQABgSADgOQAUhgADgvQAFhCgThIQgchxhwhCQhig5h3AAQhlAAheBGgABDAUQgFAAgEgDQgEgDAAgFQgGg4AShGQAjiKB2hFQAzgdAzgTQAfgLA1gOQBHgSAlgTQA8gfA+hDQBghngTiVQgGgvgRguIgQglQgDgFACgGQACgFAEgDQAFgCAGACQAFABACAFIASAoQASAyAHAyQAUChhoBxQhCBGg/AhQgnAUhLATQg0AOgeALQgwARgxAcQhtBAggB/QgRBAAFA0QABAFgDAEQgEAFgFAAg");
	this.shape.setTransform(0.0267,0.0056);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-74.7,-93.5,149.5,187);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.CachedBmp_1();
	this.instance.setTransform(-174.95,-175.6,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-174.9,-175.6,350,351);


// stage content:
(lib.logo_dra_emy = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {volta:429};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [914];
	// timeline functions:
	this.frame_914 = function() {
		var _this = this;
		/*
		Moves the playhead to the specified frame label in the timeline and continues playback from that frame.
		Can be used on the main timeline or on movie clip timelines.
		*/
		_this.gotoAndPlay('volta');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(914).call(this.frame_914).wait(1));

	// bebe
	this.instance = new lib.Tween4("synched",0);
	this.instance.setTransform(180,180);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).to({alpha:1},25).wait(394).to({startPosition:0},0).wait(435).to({startPosition:0},0).wait(50).to({startPosition:0},0).wait(1));

	// aro
	this.instance_1 = new lib.Tween1("synched",0);
	this.instance_1.setTransform(180,180,0.9714,0.9712);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:0.1,regY:0.1,rotation:-1.4918,x:180.1,y:180.1,alpha:1},6).to({regX:0,regY:0,rotation:360,x:180,y:180},423).to({rotation:720},435).wait(50).to({startPosition:0},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(119.1,119.1,301.79999999999995,301.9);
// library properties:
lib.properties = {
	id: 'DCC6D4C2C8A8473EB05F89CD377B77F1',
	width: 360,
	height: 360,
	fps: 24,
	color: "#D4C5A6",
	opacity: 1.00,
	manifest: [
		{src:"images/logo_dra_emy_atlas_1.png", id:"logo_dra_emy_atlas_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['DCC6D4C2C8A8473EB05F89CD377B77F1'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;